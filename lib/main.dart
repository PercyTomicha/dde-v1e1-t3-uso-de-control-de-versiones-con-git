import 'package:flutter/material.dart';
import 'injector.dart' as di;

import 'app.dart';

void main() {
  di.init();
  runApp(const MainApp());
}
