class Income {
  Income({
    required this.description,
    required this.amount,
    required this.date
  });

  final String description;
  final double amount;
  final String date;

}
