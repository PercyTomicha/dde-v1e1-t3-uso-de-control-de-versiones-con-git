import '../models/income.model.dart';

abstract class IncomeDataSource {
  Future<void> createIncome(IncomeModel incomeModel);
}

class IncomeService implements IncomeDataSource {
  @override
  Future<void> createIncome(IncomeModel incomeModel) async {
    await Future.delayed(const Duration(seconds: 2));
    print('IncomeService -> createIncome');
  }
}
