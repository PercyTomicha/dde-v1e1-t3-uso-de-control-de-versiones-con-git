import 'package:flutter/material.dart';

extension ContextExt on BuildContext {
  go(String routeName) => Navigator.pushNamed(this, routeName);
  
  back() => Navigator.pop(this);
}