import 'package:get_it/get_it.dart';

import 'features/incomes/data/datasources/income.datasource.dart';
import 'features/incomes/data/repositories/income.repository.impl.dart';
import 'features/incomes/domain/repositories/income.repository.dart';
import 'features/incomes/presentation/blocs/income.bloc.dart';

GetIt getIt = GetIt.instance;

void init() {
  initIncomeDependencies();
}

void initIncomeDependencies() {
  getIt.registerSingleton<IncomeDataSource>(IncomeService());
  getIt.registerSingleton<IncomeRepository>(IncomeRepositoryImpl(getIt()));
  getIt.registerFactory<IncomeBloc>(() => IncomeBloc(getIt()));
}
