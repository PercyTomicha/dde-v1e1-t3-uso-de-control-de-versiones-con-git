import 'package:flutter/material.dart';
import '../../../../core/extensions/context.dart';
import '../../../incomes/presentation/pages/income.page.dart';

class HomePage extends StatelessWidget {
  static String routeName = '/';

  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          onPressed: () => context.go(IncomePage.routeName),
          child: const Text('Ingresos')
        ),
      ),
    );
  }
}
