import '../../domain/entities/income.entity.dart';
import '../../domain/repositories/income.repository.dart';
import '../datasources/income.datasource.dart';
import '../models/income.model.dart';

class IncomeRepositoryImpl implements IncomeRepository {
  final IncomeDataSource _datasource;

  IncomeRepositoryImpl(this._datasource);

  @override
  Future<void> createIncome(Income income) async {
    await _datasource.createIncome(IncomeModel.fromEntity(income));
    await Future.delayed(const Duration(seconds: 2));
    print('IncomeRepositoryImpl -> createIncome');
  }
}
