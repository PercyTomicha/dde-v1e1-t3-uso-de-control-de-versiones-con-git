import 'package:flutter_bloc/flutter_bloc.dart';

import '../../domain/entities/income.entity.dart';
import '../../domain/repositories/income.repository.dart';

part 'income.event.dart';
part 'income.state.dart';

class IncomeBloc extends Bloc<IncomeEvent, IncomeState> {
  final IncomeRepository _repository;

  IncomeBloc(this._repository) : super(IncomeInitial()) {
    on<IncomeCreated>(onCreated);
  }

  Future<void> onCreated(IncomeCreated event, Emitter emit) async {
    try {
      emit(IncomeLoading());
      print('*****');
      await _repository.createIncome(event.income);
      emit(IncomeSuccess());
    } catch (e) {
      emit(IncomeFailure());
    }
  }
}
