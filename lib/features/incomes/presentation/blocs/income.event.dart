part of 'income.bloc.dart';

abstract class IncomeEvent {}

class IncomeCreated extends IncomeEvent {
  final Income income;

  IncomeCreated(this.income);
}
