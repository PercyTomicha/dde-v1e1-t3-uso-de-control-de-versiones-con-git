part of 'income.bloc.dart';

sealed class IncomeState {}

class IncomeInitial extends IncomeState {
  IncomeInitial();
}

class IncomeLoading extends IncomeState {
  IncomeLoading();
}

class IncomeSuccess extends IncomeState {
  IncomeSuccess();
}

class IncomeFailure extends IncomeState {
  IncomeFailure();
}