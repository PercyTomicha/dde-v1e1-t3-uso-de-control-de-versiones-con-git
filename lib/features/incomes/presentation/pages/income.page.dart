import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/extensions/context.dart';
import '../../../../injector.dart';
import '../../domain/entities/income.entity.dart';
import '../blocs/income.bloc.dart';

class IncomePage extends StatelessWidget {
  static String routeName = '/income';

  const IncomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<IncomeBloc>(
        create: (_) => getIt<IncomeBloc>(),
        child: Center(
          child: BlocBuilder<IncomeBloc, IncomeState>(
            builder: (context, state) {
              if (state is IncomeLoading) {
                print('state is IncomeLoading');
                return const Center(child: CircularProgressIndicator());
              }
              if (state is IncomeSuccess) {
                print('state is IncomeSuccess');
                return Column(
                  children: [
                    const Text('Success'),
                    Center(
                      child: ElevatedButton(
                        onPressed: () => context.back(),
                        child: const Text('Back')
                      ),
                    ),
                  ],
                );
              }
              if (state is IncomeFailure) {
                print('state is IncomeFailure');
                return const Text('Failure');
              }
              return InkWell(
                child: const Text('Income Page'),
                onTap: () =>
                    context.read<IncomeBloc>().add(IncomeCreated(Income(
                          description: 'Descripción',
                          amount: 20.0,
                          date: '22/04/1997',
                        ))),
              );
            },
          ),
        ),
      ),
    );
  }
}
