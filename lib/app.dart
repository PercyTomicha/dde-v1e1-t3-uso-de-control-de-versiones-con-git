import 'package:flutter/material.dart';

import 'features/home/presentation/pages/home.page.dart';
import 'features/incomes/presentation/pages/income.page.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // darkTheme: ,
      // theme: ,
      // themeMode: ,
      initialRoute: HomePage.routeName,
      routes: {
        HomePage.routeName : (context) => const HomePage(),
        IncomePage.routeName :(context) => const IncomePage()
      },
    );
  }
}
