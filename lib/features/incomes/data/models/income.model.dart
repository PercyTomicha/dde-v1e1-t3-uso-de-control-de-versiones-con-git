import '../../domain/entities/income.entity.dart';

class IncomeModel extends Income {
  IncomeModel(
      {required super.description, required super.amount, required super.date});

  factory IncomeModel.fromEntity(Income income) {
    return IncomeModel(
      description: income.description,
      amount: income.amount,
      date: income.date
    );
  }
}
