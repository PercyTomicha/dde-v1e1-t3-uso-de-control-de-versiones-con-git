import '../entities/income.entity.dart';

abstract class IncomeRepository {
  Future<void> createIncome(Income income);
}
